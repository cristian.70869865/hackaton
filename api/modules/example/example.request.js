const exampleModel = ['name', 'lastname'];

module.exports = (exampleBody) => {
  let modelRequest = {};

  for (let field of exampleModel) {
    if (exampleBody[field] === undefined)
      modelRequest[field] = null;
    if (exampleBody[field] !== undefined)
      modelRequest[field] = exampleBody[field]
  }

  return modelRequest;
}