const ExampleHttpResponse = require('./example.httpResponse');

function validationToGetAllExample (examples) {
  if (!examples.length) {
    return ExampleHttpResponse.notFound();
  }

  return {
    code: 200,
    data: examples
  }
}

function validationToPostExample (exampleCreated) {
  console.log('validate', exampleCreated);
  return ExampleHttpResponse.createdSuccessfully();
}

module.exports = {
  validationToGetAllExample,
  validationToPostExample
}