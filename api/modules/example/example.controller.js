const ExampleRequest  = require('./example.request');
const ExampleService  = require('./example.service');
const ExampleValidate = require('./example.validate');
const ExampleError    = require('./example.error');

module.exports = Object.freeze({
  getAllExamples,
  postExample
});

async function getAllExamples (req, res) {
  let examples = [];
  let httpResponse = {};

  try {
    examples = await ExampleService.getAllExamples();
    httpResponse = ExampleValidate.validationToGetAllExample(examples);

    return res.status(httpResponse.code)
      .send(httpResponse);
  } catch (error) {
    httpResponse = ExampleError(error);

    return res.status(httpResponse.code)
      .send(httpResponse);
  }
}

async function postExample (req, res) {
  let exampleRequest  = {};
  let httpResponse    = {};

  try {
    exampleRequest = ExampleRequest(req.body);
    await ExampleService.postExample(exampleRequest);
    httpResponse = ExampleValidate.validationToPostExample();

    return res.status(httpResponse.code)
      .send(httpResponse);
  } catch (error) {
    httpResponse = ExampleError(error);

    return res.status(httpResponse.code)
      .send(httpResponse);
  }
}