const ExampleController = require('./example.controller');

module.exports = (router) => {
  let route = router.Router();

  defineRoutes(route);
  return route;
}

function defineRoutes (route) {
  route.get('/example', ExampleController.getAllExamples);
  route.post('/example', ExampleController.postExample);
}