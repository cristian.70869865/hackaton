function createdSuccessfully () {
  return {
    code: 201,
    data: {
      message: "Se creo con exito"
    }
  }
}

function badRequest () {
  return {
    code: 400,
    data: {
      message: "Datos incorrectos"
    }
  }
}

function notFound () {
  return {
    code: 404,
    data: {
      message: "No se encontro ningun ejemplo"
    }
  }
}

function internalServerError () {
  return {
    code: 500,
    data: {
      message: "Problema interno del servidor"
    }
  }
}

module.exports = {
  createdSuccessfully,
  badRequest,
  notFound,
  internalServerError
}