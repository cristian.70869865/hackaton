const ExampleHttpResponse = require('./example.httpResponse');

module.exports = function (error) {
  let errorName = error.name;

  if (errorName === 'CastError') 
    return castError(errorName);  
  if (errorName === 'ValidationError') 
    return validationError(error.message);   
  if (errorName === 'ObjectParameterError') 
    return objectParameterError (error.message);           
  if(errorName === 'MongoError') 
    return mongoError(error.message)
    
  return ExampleHttpResponse.internalServerError();
};

function castError (errorMessage) {
  if (errorMessage.includes('ObjectId'))
    return ExampleHttpResponse.notFound();
}

function validationError (errorMessage) {
  if (errorMessage.includes('required'))
    return ExampleHttpResponse.badRequest();
}

function objectParameterError (errorMessage) {
  if (errorMessage.includes('Document()'))
    return ExampleHttpResponse.notFound();
}

function mongoError (errorMessage) {
  if (errorMessage.includes('$regex'))
    return ExampleHttpResponse.badRequest();
  if (errorMessage.includes('duplicate key'))
    return StatusHttpResponse.duplicateKey();
}
