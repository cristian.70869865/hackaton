const ExampleSchema = require('../../schemas/example');

async function getAllExamples () {
  return await ExampleSchema.find({});
}

async function postExample (exampleResquest) {
  return await ExampleSchema.create(exampleResquest);
}

module.exports = {
  getAllExamples,
  postExample
}