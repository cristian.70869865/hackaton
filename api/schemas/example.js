const mongoose = require('mongoose');
const schema   = mongoose.Schema;
const exampleSchema = schema({
    name: {
      type: String, 
      require: true
    },
    lastname: {
      type: String, 
      required: true
    }
});

module.exports = mongoose.model('example', exampleSchema);
