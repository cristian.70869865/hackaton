const MongoClient = require('mongodb').MongoClient;

let url = 'mongodb://localhost';
let dbName = 'cheatApp';
let collections = [
  { name: "example", unique: { name: 1, lastname: 1 } }
];

MongoClient.connect(url, function(error, client) {
  if (error)
    console.log(error);
  if (!error) {
    let db = client.db(dbName);

    for (let item of collections)
      createUniqueIndex(db.collection(item.name), item.unique)

    client.close();
  }
});

function createUniqueIndex (collection, uniqueIndex) {
  collection.createIndex(uniqueIndex, { unique: true });
}