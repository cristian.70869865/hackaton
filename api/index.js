const express = require('express');
const app = require('./app')(express);
const mongoose = require('mongoose');

dbConnect();

function dbConnect () {
  let stringConnection = 'mongodb://localhost/cheatApp';

  mongoose.connect(stringConnection, {
    autoIndex: false,
    useNewUrlParser: true
  })

  mongoose.connection.on('connected', (data) => {
    init();
  })

  mongoose.connection.on('error', (error) => {
    console.log('db error', error);
  })

  mongoose.connection.on('disconnected', (disconnected) => {
    console.log('db disconnected', disconnected);
  })

  process.on('SIGINT', () => {
    mongoose.connection.close( () => {
        console.log(termination("Mongoose default connection is disconnected due to application termination"));
        process.exit(0)
    });
  });
}

async function init () {
  app.listen(process.env.PORT || 500, () => {
    console.log('API Rest corriendo.')
  });
}