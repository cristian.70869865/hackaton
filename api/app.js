const BodyParser = require('body-parser');
const ExampleRoute = require('./modules/example/example.route');

module.exports = (express) => {
  let app   = express();
  let route = express;

  app.use(BodyParser.urlencoded( {extended: false} ));
  app.use(BodyParser.json());
  defineCors(app);
  routesUsedForVersionOne(app, route);

  return app;
}

function defineCors (app) {
  app.all('/*', (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, " +
        "Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    next();
  });
}

function routesUsedForVersionOne (app, route) {
  app.use('/api/v1', ExampleRoute(route));
}